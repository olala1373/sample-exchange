import org.mrb.paysera.dependencies.Dependencies

plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
    id("kotlin-android-extensions")
}

android {
    compileSdk = 31

    defaultConfig {
        minSdk = 24
        targetSdk = 31

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {

    implementation(Dependencies.BaseLibs.coreKtx)
    implementation(Dependencies.BaseLibs.appCompat)
    implementation(Dependencies.BaseLibs.coroutine)
    implementation(Dependencies.BaseLibs.javaxInject)
    implementation(Dependencies.BaseLibs.kotlinStdLib)
    testImplementation(Dependencies.BaseLibs.junitTest)
    androidTestImplementation(Dependencies.Test.junit)
    androidTestImplementation(Dependencies.Test.espresso)
}