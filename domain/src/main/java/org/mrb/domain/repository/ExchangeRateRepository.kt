package org.mrb.domain.repository

import kotlinx.coroutines.flow.Flow
import org.mrb.domain.models.MResult
import org.mrb.domain.models.Rate

interface ExchangeRateRepository {
    suspend fun getRates() : Flow<MResult<Rate?>>
}