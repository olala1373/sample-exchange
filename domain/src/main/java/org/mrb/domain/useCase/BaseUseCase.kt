package org.mrb.domain.useCase

interface BaseUseCase<in Params , out T> {
    suspend fun execute(params: Params) : T
}