package org.mrb.domain.useCase

import kotlinx.coroutines.flow.Flow
import org.mrb.domain.models.MResult
import org.mrb.domain.models.Rate
import org.mrb.domain.repository.ExchangeRateRepository
import javax.inject.Inject

class GetRatesUseCase @Inject constructor(private val repository: ExchangeRateRepository) :
    BaseUseCase<Unit, Flow<@kotlin.jvm.JvmSuppressWildcards MResult<Rate?>>> {
    override suspend fun execute(params: Unit): Flow<MResult<Rate?>> {
        return repository.getRates()
    }
}