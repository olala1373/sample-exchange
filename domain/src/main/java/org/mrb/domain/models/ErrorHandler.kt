package org.mrb.domain.models

interface ErrorHandler {
    fun getError(throwable: Throwable): MResult.ErrorType
    fun getApiError(statusCode: Int, throwable: Throwable? = null): MResult.ErrorType
}
