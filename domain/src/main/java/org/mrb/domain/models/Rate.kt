package org.mrb.domain.models

data class Rate(
    val base : String,
    val date : String,
    val items : List<RateItem>
)