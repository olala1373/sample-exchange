package org.mrb.domain.models

sealed class MResult<out T>(
    val data: T? = null,
    val error: ErrorType? = null
) {
    class Success<T>(data: T) : MResult<T>(data)
    class Loading<T>(data: T? = null) : MResult<T>(data)
    class Error<T>(error: ErrorType? = null, data: T? = null) : MResult<T>(data, error)

    sealed class ErrorType(
        val throwable: Throwable? = null,
        val code: Int? = null,
        val message: String? = null
    ) {
        class DatabaseError(throwable: Throwable? = null) : ErrorType(throwable)
        class IOError(throwable: Throwable? = null) : ErrorType(throwable)
        class HttpError(throwable: Throwable? = null, statusCode: Int) :
            ErrorType(throwable, statusCode)

        class Unknown(throwable: Throwable? = null) : ErrorType(throwable)
        class ErrorObject(message: String? = null, statusCode: Int) :
            ErrorType(message = message, code = statusCode)
    }
}