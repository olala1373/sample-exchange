package org.mrb.domain.models

data class RateItem(
    val name : String,
    val value : Double
)