package org.mrb.paysera.dependencies

object Dependencies {
    object Plugins {
        const val androidApplication = "com.android.application"
        const val kotlinAndroid = "kotlin-android"
        const val kotlinAndroidExtensions = "kotlin-android-extensions"
        const val kotlinKapt = "kotlin-kapt"
        const val hiltPlugin = "dagger.hilt.android.plugin"
    }

    object Config {
        const val compileSdkVersion = 30
        const val buildToolsVersion = "30.0.3"
        const val applicationId = "org.pool.r2pool"
        const val minSdkVersion = 23
        const val targetSdkVersion = 30
        const val versionCode = 1
        const val versionName = "1.0.0"
        const val testInstrumentation = "androidx.test.runner.AndroidJUnitRunner"
    }

    object ClassPaths {
        object Versions {
            const val gradleVersion = "7.0.0-alpha12"
            const val kotlinGradleVersion = "1.4.30"
        }

        const val buildGradleTool = "com.android.tools.build:gradle:${Versions.gradleVersion}"
        const val kotlinGradlePlugin =
            "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlinGradleVersion}"
        const val hiltAndroidPlugin =
            "com.google.dagger:hilt-android-gradle-plugin:_"
        const val navigationSafeArgs = "androidx.navigation:navigation-safe-args-gradle-plugin:_"
        const val firebaseCrashlytics = "com.google.firebase:firebase-crashlytics-gradle:_"
        const val googleServices = "com.google.gms:google-services:_"
    }


    object BaseLibs {
        object Versions {
            const val coreVersion = "1.7.0"
            const val appCompatVersion = "1.4.1"
            const val fragmentVersion = "1.4.1"
            const val constraintLayoutVersion = "2.1.3"
            const val materialVersion = "1.5.0"
            const val viewBindingVersion = "7.1.2"
            const val koltinxCoroutines = "1.6.0"
            const val junitVersion = "4.13.2"
            const val javaxInjectVersion = "1"
        }

        private const val kotlinVersion = "1.4.21"
        const val kotlinStdLib = "org.jetbrains.kotlin:kotlin-stdlib:${kotlinVersion}"
        const val coreKtx = "androidx.core:core-ktx:${Versions.coreVersion}"
        const val appCompat = "androidx.appcompat:appcompat:${Versions.appCompatVersion}"
        const val material = "com.google.android.material:material:${Versions.materialVersion}"
        const val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayoutVersion}"
        const val fragment = "androidx.fragment:fragment-ktx:${Versions.fragmentVersion}"
        const val viewBinding = "com.android.databinding:viewbinding:${Versions.viewBindingVersion}"
        const val coroutine =
            "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.koltinxCoroutines}"
        const val junitTest = "junit:junit:${Versions.junitVersion}"
        const val javaxInject = "javax.inject:javax.inject:${Versions.javaxInjectVersion}"
    }

    object LifeCycle {
        private const val lifeCycleVersion = "2.4.1"
        const val viewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:$lifeCycleVersion"
        const val liveData = "androidx.lifecycle:lifecycle-livedata-ktx:$lifeCycleVersion"
        const val runTime = "androidx.lifecycle:lifecycle-runtime-ktx:$lifeCycleVersion"
        const val commonJava8 =
            "androidx.lifecycle:lifecycle-common-java8:$lifeCycleVersion"

    }

    object Room {
        object Versions {
            const val roomVersion = "2.4.2"
        }

        const val roomRuntime = "androidx.room:room-runtime:${Versions.roomVersion}"
        const val roomCompiler = "androidx.room:room-compiler:${Versions.roomVersion}"
        const val roomKtx = "androidx.room:room-ktx:${Versions.roomVersion}"
    }
    object Navigation {
        object Versions {
            const val navVersion = "2.4.1"
        }

        const val navigationFragmentKtx =
            "androidx.navigation:navigation-fragment-ktx:${Versions.navVersion}"
        const val navigationUIKtx =
            "androidx.navigation:navigation-ui-ktx:${Versions.navVersion}"
        const val navigationDynamicFeatures =
            "androidx.navigation:navigation-dynamic-features-fragment:${Versions.navVersion}"
    }


    object Hilt {
        object Versions {
            const val hiltViewModelVersion = "1.0.0-alpha03"
            const val hiltVersion = "2.38.1"
        }

        const val hilt = "com.google.dagger:hilt-android:${Versions.hiltVersion}"
        const val hiltKaptCompiler =
            "com.google.dagger:hilt-android-compiler:${Versions.hiltVersion}"
        const val hiltViewModel =
            "androidx.hilt:hilt-lifecycle-viewmodel:${Versions.hiltViewModelVersion}"
        const val hiltViewModelKapt =
            "androidx.hilt:hilt-compiler:1.0.0"
        const val hiltViewModelNavigation = "androidx.hilt:hilt-navigation-fragment:1.0.0"
    }

    object Test {
        object Versions {
            const val jUnitVersion = "1.1.3"
            const val espressoCore = "3.4.0"
        }

        const val junit = "androidx.test.ext:junit:1.1.3"
        const val espresso = "androidx.test.espresso:espresso-core:3.4.0"
    }
    object Retrofit {
        object Versions {
            const val retrofitVersion = "2.9.0"
            const val interceptorVersion = "4.9.3"
            const val rxJavaVersion = "2.4.0"
        }

        const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofitVersion}"
        const val gsonConvertor =
            "com.squareup.retrofit2:converter-gson:${Versions.retrofitVersion}"
        const val loggingInterceptor =
            "com.squareup.okhttp3:logging-interceptor:${Versions.interceptorVersion}"
        const val rxJavaAdapter =
            "com.squareup.retrofit2:adapter-rxjava2:${Versions.rxJavaVersion}"
    }
    object UI {
        object Versions {
            const val loadingProgressVersion = "2.2.0"
        }
        const val loadingProgressButton =
            "br.com.simplepass:loading-button-android:${Versions.loadingProgressVersion}"
        const val swipeRefreshLayout = "androidx.swiperefreshlayout:swiperefreshlayout:1.1.0"
    }



}