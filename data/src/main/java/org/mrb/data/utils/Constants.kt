package org.mrb.data.utils

object Constants {

    const val SERA_PREF = "paysera pref"
    object AppConfig {
        val currencyList = listOf("JPY" , "USD" , "GBP")
        const val BASE = "EUR"
        const val API_CALL_INTERVAL : Long = 500 * 1000 //this is 5 Seconds interval i changed to 500 second for api restriction call
        const val DEFAULT_COMMISSION_FREE = 5
        const val FEE = 0.7
        const val DEFAULT_BALANCE = 1000
    }

    object API{
        const val BASE_URL="http://api.exchangeratesapi.io/v1/"
        const val ACCESS_KEY="7140a4dc7c2d5e1ba2205a50f9a48dec"
        const val API_REQUEST_TIMEOUT = 10L
    }

    object Database {
        const val VERSION = 5
        const val NAME = "PaySeraDB"
    }
}