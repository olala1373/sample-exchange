package org.mrb.data.repository

import org.mrb.data.database.dao.CurrencyDao
import org.mrb.data.database.entity.CurrencyEntity
import javax.inject.Inject

class CurrencyRepository @Inject constructor(private val dao : CurrencyDao){

    suspend fun insertCurrency(entity: CurrencyEntity){
        dao.insert(entity)
    }

    fun getCurrencies() = dao.getCurrencies()

    suspend fun insertCurrencies(list : List<CurrencyEntity>){
        dao.insertAll(list)
    }

    suspend fun updateCurrency(entity: CurrencyEntity) {
        dao.update(entity)
    }

    suspend fun updateCurrencyBalance(id : Int , balance : Double){
        dao.updateCurrencyById(id , balance)
    }

    suspend fun updateCurrencyBalanceByName(name : String , balance : Double){
        dao.updateCurrencyByName(name , balance)
    }
}