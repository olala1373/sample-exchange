package org.mrb.data.repository

import androidx.annotation.WorkerThread
import org.mrb.data.database.dao.HistoryDao
import org.mrb.data.database.entity.HistoryEntity
import javax.inject.Inject

class HistoryRepository @Inject constructor(private val dao: HistoryDao) {

    fun getHistories() = dao.getAllHistories()

    @WorkerThread
    suspend fun insertHistory(entity : HistoryEntity){
        dao.insert(entity)
    }

    fun getBalanceTraded() = dao.getTotalTransactions()

}