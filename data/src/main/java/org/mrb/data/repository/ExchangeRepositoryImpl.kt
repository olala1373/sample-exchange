package org.mrb.data.repository

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import org.mrb.data.di.IoDispatcher
import org.mrb.data.models.ErrorHandlerImpl
import org.mrb.data.models.RateDto
import org.mrb.data.remote.ExchangeAPI
import org.mrb.data.remote.NetworkBoundResource
import org.mrb.data.utils.Constants
import org.mrb.domain.models.MResult
import org.mrb.domain.models.Rate
import org.mrb.domain.models.RateItem
import org.mrb.domain.repository.ExchangeRateRepository
import retrofit2.Response
import javax.inject.Inject

class ExchangeRepositoryImpl @Inject constructor(
    private val api: ExchangeAPI,
    private val errorHandlerImpl: ErrorHandlerImpl,
    @IoDispatcher private val dispatcher: CoroutineDispatcher
) : ExchangeRateRepository {
    override suspend fun getRates(): Flow<MResult<Rate?>> =
        object : NetworkBoundResource<RateDto, Rate?>(errorHandlerImpl) {
            override fun sendDataToUI(remoteResponse: RateDto): Flow<Rate?> {
                return flow {
                    val rate = Rate(
                        base = remoteResponse.base,
                        date = remoteResponse.date,
                        items = extractRates(remoteResponse)
                    )
                    emit(rate)
                }
            }

            override suspend fun fetchFromRemote(): Response<RateDto> {
                return api.getRates()
            }

        }.asFlow().flowOn(dispatcher)

    private fun extractRates(rate: RateDto): List<RateItem> {
        val availableCurrencies = Constants.AppConfig.currencyList
        val rateItems = mutableListOf<RateItem>()
        run loop@{
            rate.rates.javaClass.declaredFields.forEach fields@{
                val existCurrency =
                    availableCurrencies.find { cur -> it.name.uppercase() == cur.uppercase() }
                if (existCurrency != null) {
                    it.isAccessible = true
                    rateItems.add(RateItem(existCurrency, it.get(rate.rates) as Double))
                }
                if (rateItems.size == availableCurrencies.size)
                    return@loop
            }
        }
        return rateItems.toList()
    }
}