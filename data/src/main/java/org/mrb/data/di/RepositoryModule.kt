package org.mrb.data.di

import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import org.mrb.data.database.dao.CurrencyDao
import org.mrb.data.database.dao.HistoryDao
import org.mrb.data.repository.CurrencyRepository
import org.mrb.data.repository.ExchangeRepositoryImpl
import org.mrb.data.repository.HistoryRepository
import org.mrb.domain.repository.ExchangeRateRepository


@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun bindExchangeRepo(exchangeRepositoryImpl: ExchangeRepositoryImpl) : ExchangeRateRepository

    companion object{
        @Provides
        fun provideCurrencyRepo(dao : CurrencyDao) : CurrencyRepository = CurrencyRepository(dao)

        @Provides
        fun provideHistoryRepo(dao : HistoryDao) : HistoryRepository = HistoryRepository(dao)
    }
}