package org.mrb.data.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import org.mrb.data.remote.ExchangeAPI
import retrofit2.Retrofit

@Module
@InstallIn(SingletonComponent::class)
object APIModule {

    @Provides
    fun provideGetRateAPI(retrofit: Retrofit) : ExchangeAPI =
        retrofit.create(ExchangeAPI::class.java)
}