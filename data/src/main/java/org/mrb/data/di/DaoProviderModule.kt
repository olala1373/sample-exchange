package org.mrb.data.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import org.mrb.data.database.AppDatabase
import org.mrb.data.database.dao.CurrencyDao
import org.mrb.data.database.dao.HistoryDao

@Module
@InstallIn(SingletonComponent::class)
object DaoProviderModule {
    @Provides
    fun provideCurrencyDao(appDatabase: AppDatabase) : CurrencyDao = appDatabase.currencyDao()

    @Provides
    fun provideHistoryDao(appDatabase: AppDatabase) : HistoryDao = appDatabase.historyDao()
}