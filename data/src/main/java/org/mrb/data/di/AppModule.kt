package org.mrb.data.di

import android.content.Context
import androidx.room.Room
import com.google.gson.GsonBuilder
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.mrb.data.BuildConfig
import org.mrb.data.database.AppDatabase
import org.mrb.data.models.ErrorHandlerImpl
import org.mrb.data.remote.AccessKeyInterceptor
import org.mrb.data.utils.Constants
import org.mrb.domain.models.ErrorHandler
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class AppModule {

    @Binds
    abstract fun accessKeyInterceptor(accessKeyInterceptor: AccessKeyInterceptor) : Interceptor

    @Binds
    abstract fun bindErrorHandler(errorHandlerImpl: ErrorHandlerImpl): ErrorHandler

    companion object {

        @Provides
        @Singleton
        fun provideLocalDatabase(@ApplicationContext context: Context) =
            Room.databaseBuilder(
                context,
                AppDatabase::class.java,
                Constants.Database.NAME
            ).fallbackToDestructiveMigration().build()


        @Singleton
        @Provides
        fun provideRetrofit(client: OkHttpClient): Retrofit =
            Retrofit.Builder()
                .baseUrl(Constants.API.BASE_URL)
                .client(client)
                .addConverterFactory(
                    GsonConverterFactory.create(
                        GsonBuilder().setLenient().create()
                    )
                )
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

        @Provides
        fun provideOkHttpClient(interceptor: Interceptor): OkHttpClient {
            val builder = OkHttpClient().newBuilder().addInterceptor(interceptor)
            if (BuildConfig.DEBUG) builder.addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
            builder
                .connectTimeout(Constants.API.API_REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(Constants.API.API_REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(Constants.API.API_REQUEST_TIMEOUT, TimeUnit.SECONDS)

            return builder.build()
        }

    }
}