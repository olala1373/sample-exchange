package org.mrb.data.database

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import org.mrb.data.utils.Constants
import javax.inject.Inject

class SharedPref @Inject constructor(
    private val sharedPrefManager: SharedPrefManager,
    @ApplicationContext private val context: Context
) {

    fun setBalance(balance: Double) {
        sharedPrefManager.setLongValue(balanceKey, balance.toRawBits())
    }

    fun getBalance() =
        Double.fromBits(sharedPrefManager.getLongValue(balanceKey , 0L))

    fun setCommissionFreeRule(count : Int) {
        sharedPrefManager.setIntValue(commissionFreeKey , count)
    }

    fun getCommissionFreeCount() = sharedPrefManager.getIntValue(commissionFreeKey , Constants.AppConfig.DEFAULT_COMMISSION_FREE)

    companion object {
        const val balanceKey = "Balance Key"
        const val commissionFreeKey = "Commission Key"
    }


}
