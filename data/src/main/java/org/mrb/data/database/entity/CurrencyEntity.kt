package org.mrb.data.database.entity

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    indices = [Index(value = ["name"] , unique = true)]
)
data class CurrencyEntity(
    val name : String,
    val balance : Double
){
    @PrimaryKey(autoGenerate = true)
    var id : Int = 0
}