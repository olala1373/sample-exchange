package org.mrb.data.database.dao

import androidx.room.Dao
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import org.mrb.data.database.BaseDao
import org.mrb.data.database.entity.CurrencyEntity

@Dao
interface CurrencyDao : BaseDao<CurrencyEntity> {

    @Query("Select * From CurrencyEntity")
    fun getCurrencies() : Flow<List<CurrencyEntity>>

    @Query("Update CurrencyEntity set balance = :balance Where id = :id" )
    suspend fun updateCurrencyById(id : Int , balance : Double)

    @Query("Update CurrencyEntity set balance = balance + :balance Where name = :name" )
    suspend fun updateCurrencyByName(name : String , balance : Double)
}