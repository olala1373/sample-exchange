package org.mrb.data.database.dao

import androidx.room.Dao
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import org.mrb.data.database.BaseDao
import org.mrb.data.database.entity.HistoryEntity

@Dao
interface HistoryDao : BaseDao<HistoryEntity> {

    @Query("Select * From HistoryEntity")
    fun getAllHistories() : Flow<List<HistoryEntity>>

    @Query("Select SUM(fromAmount + fee) from HistoryEntity")
    fun getTotalTransactions() : Flow<Double?>
}