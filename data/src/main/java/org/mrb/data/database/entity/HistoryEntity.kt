package org.mrb.data.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class HistoryEntity(
    val fromCurrency : String,
    val toCurrency : String,
    val fromAmount : Double,
    val toAmount : Double,
    val rate : Double,
    val fee : Double,
    val createdDate : String
){
    @PrimaryKey(autoGenerate = true)
    var id : Int = 0
}
