package org.mrb.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import org.mrb.data.database.dao.CurrencyDao
import org.mrb.data.database.dao.HistoryDao
import org.mrb.data.database.entity.CurrencyEntity
import org.mrb.data.database.entity.HistoryEntity
import org.mrb.data.utils.Constants

@Database(
    entities = [
        CurrencyEntity::class,
        HistoryEntity::class
               ],
    version = Constants.Database.VERSION
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun currencyDao(): CurrencyDao
    abstract fun historyDao(): HistoryDao
}