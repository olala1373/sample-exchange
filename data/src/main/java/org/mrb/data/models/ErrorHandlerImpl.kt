package org.mrb.data.models

import org.mrb.domain.models.ErrorHandler
import org.mrb.domain.models.MResult
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class ErrorHandlerImpl @Inject constructor() : ErrorHandler {

    override fun getError(throwable: Throwable): MResult.ErrorType {
        return when (throwable) {
            is IOException -> MResult.ErrorType.IOError(throwable)
            is HttpException -> MResult.ErrorType.HttpError(throwable, throwable.code())
            else -> MResult.ErrorType.Unknown(throwable)
        }
    }

    override fun getApiError(statusCode: Int, throwable: Throwable?): MResult.ErrorType {
        return MResult.ErrorType.HttpError(throwable, statusCode)
    }
}
