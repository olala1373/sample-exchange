package org.mrb.data.models


import com.google.gson.annotations.SerializedName

data class RateDto(
    val base: String,
    val date: String,
    val rates: Rates,
    val success: Boolean,
    val timestamp: Long
)