package org.mrb.data.remote

import org.mrb.data.models.RateDto
import retrofit2.Response
import retrofit2.http.GET

interface ExchangeAPI {
    @GET("latest")
    suspend fun getRates() : Response<RateDto>
}