package org.mrb.data.remote

import okhttp3.Interceptor
import okhttp3.Response
import org.mrb.data.utils.Constants
import javax.inject.Inject

class AccessKeyInterceptor @Inject constructor() : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val originalUrl = original.url
        val url = originalUrl.newBuilder()
            .addQueryParameter("access_key" , Constants.API.ACCESS_KEY)
            .build()
        val requestBuilder = original.newBuilder()
            .url(url)
        val request = requestBuilder.build()
        return chain.proceed(request)
    }
}