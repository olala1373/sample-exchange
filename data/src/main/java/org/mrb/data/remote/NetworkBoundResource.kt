package org.mrb.data.remote

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import org.mrb.domain.models.ErrorHandler
import org.mrb.domain.models.MResult
import retrofit2.Response

abstract class NetworkBoundResource<RequestType, ResultType>(private val errorHandler: ErrorHandler) {

    fun asFlow() = flow {
        emit(MResult.Loading(null))
        try {
            val apiResponse = fetchFromRemote()
            val remoteResponse = apiResponse.body()
            if (apiResponse.isSuccessful && remoteResponse != null) {
                emitAll(sendDataToUI(remoteResponse).map { MResult.Success(it) })
            } else {
                emit(
                    MResult.Error(errorHandler.getApiError(apiResponse.code(), null))
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
            emit(
                MResult.Error(errorHandler.getError(e))
            )
        }
    }


    @MainThread
    protected abstract fun sendDataToUI(remoteResponse: RequestType): Flow<ResultType>

    @MainThread
    protected abstract suspend fun fetchFromRemote(): Response<RequestType>


}