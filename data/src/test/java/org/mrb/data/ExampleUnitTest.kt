package org.mrb.data

import com.google.gson.Gson
import org.junit.Test

import org.junit.Assert.*
import org.mrb.data.models.RateDto
import org.mrb.data.utils.Constants

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    private val mockResponseJson =  "{\"success\":true,\"timestamp\":1646836923,\"base\":\"EUR\",\"date\":\"2022-03-09\",\"rates\":{\"AED\":4.054298,\"AFN\":97.825258,\"ALL\":125.873947,\"AMD\":564.289241,\"ANG\":1.989732,\"AOA\":524.334195,\"ARS\":119.959066,\"AUD\":1.506122,\"AWG\":1.98712,\"AZN\":1.871012,\"BAM\":1.968245,\"BBD\":2.22914,\"BDT\":94.974931,\"BGN\":1.960953,\"BHD\":0.416195,\"BIF\":2207.785546,\"BMD\":1.103802,\"BND\":1.503227,\"BOB\":7.601307,\"BRL\":5.522757,\"BSD\":1.104054,\"BTC\":2.6219726e-5,\"BTN\":84.807082,\"BWP\":12.837758,\"BYN\":3.623555,\"BYR\":21634.523245,\"BZD\":2.225417,\"CAD\":1.414793,\"CDF\":2225.26563,\"CHF\":1.024008,\"CLF\":0.032077,\"CLP\":885.105602,\"CNY\":6.974157,\"COP\":4155.473128,\"CRC\":716.875179,\"CUC\":1.103802,\"CUP\":29.250758,\"CVE\":110.966932,\"CZK\":25.233984,\"DJF\":196.549733,\"DKK\":7.443628,\"DOP\":60.555484,\"DZD\":157.313984,\"EGP\":17.33908,\"ERN\":16.557055,\"ETB\":56.629389,\"EUR\":1,\"FJD\":2.326264,\"FKP\":0.80348,\"GBP\":0.837615,\"GEL\":3.802586,\"GGP\":0.80348,\"GHS\":7.783737,\"GIP\":0.803479,\"GMD\":58.887783,\"GNF\":9887.544609,\"GTQ\":8.509225,\"GYD\":230.982733,\"HKD\":8.632059,\"HNL\":27.184356,\"HRK\":7.565442,\"HTG\":116.944859,\"HUF\":379.34379,\"IDR\":15761.412466,\"ILS\":3.602396,\"IMP\":0.803479,\"INR\":84.379038,\"IQD\":1611.389201,\"IRR\":46746.023421,\"ISK\":145.348393,\"JEP\":0.80348,\"JMD\":169.54889,\"JOD\":0.782604,\"JPY\":127.733048,\"KES\":125.960262,\"KGS\":107.835403,\"KHR\":4483.498323,\"KMF\":497.044187,\"KPW\":993.422159,\"KRW\":1353.443572,\"KWD\":0.335203,\"KYD\":0.920095,\"KZT\":564.098257,\"LAK\":12665.55822,\"LBP\":1669.298817,\"LKR\":251.168891,\"LRD\":169.930387,\"LSL\":16.832894,\"LTL\":3.259241,\"LVL\":0.667679,\"LYD\":5.147468,\"MAD\":10.942117,\"MDL\":20.342309,\"MGA\":4468.676069,\"MKD\":62.076661,\"MMK\":1963.14663,\"MNT\":3155.584147,\"MOP\":8.893333,\"MRO\":394.057198,\"MUR\":47.407639,\"MVR\":17.054115,\"MWK\":887.939658,\"MXN\":23.238425,\"MYR\":4.621618,\"MZN\":70.455475,\"NAD\":16.833209,\"NGN\":459.004716,\"NIO\":39.486704,\"NOK\":9.81321,\"NPR\":135.691412,\"NZD\":1.612291,\"OMR\":0.424995,\"PAB\":1.104054,\"PEN\":4.129673,\"PGK\":3.890812,\"PHP\":57.427533,\"PKR\":197.898957,\"PLN\":4.775904,\"PYG\":7676.650746,\"QAR\":4.018938,\"RON\":4.95126,\"RSD\":117.689145,\"RUB\":132.152743,\"RWF\":1120.14529,\"SAR\":4.141615,\"SBD\":8.887389,\"SCR\":15.912691,\"SDG\":492.850231,\"SEK\":10.714475,\"SGD\":1.500795,\"SHP\":1.520374,\"SLL\":12842.738765,\"SOS\":644.620584,\"SRD\":22.646703,\"STD\":22846.477105,\"SVC\":9.660848,\"SYP\":2772.750895,\"SZL\":16.749794,\"THB\":36.458576,\"TJS\":12.459514,\"TMT\":3.863308,\"TND\":3.265601,\"TOP\":2.506724,\"TRY\":16.158341,\"TTD\":7.495945,\"TWD\":31.272041,\"TZS\":2556.138836,\"UAH\":33.176963,\"UGX\":3999.426287,\"USD\":1.103802,\"UYU\":47.258655,\"UZS\":12034.517435,\"VEF\":236026271212.17972,\"VND\":25208.082889,\"VUV\":125.470758,\"WST\":2.88351,\"XAF\":660.081579,\"XAG\":0.049018,\"XAU\":0.000611,\"XCD\":2.983081,\"XDR\":0.799798,\"XOF\":660.120696,\"XPF\":120.979229,\"YER\":276.303101,\"ZAR\":16.639123,\"ZMK\":9935.548194,\"ZMW\":20.011019,\"ZWL\":355.42386}}"

    @Test
    fun testCheckingFields(){
        val dataClass = Gson().fromJson<RateDto>(mockResponseJson , RateDto::class.java)

        dataClass.rates.javaClass.declaredFields.forEach {
            val existRate = Constants.AppConfig.currencyList.find { cur -> it.name.uppercase() == cur.uppercase() }
            if (existRate != null){
                it.isAccessible = true
                println("$existRate and the value is ${it.get(dataClass.rates)}")
            }
        }
        assertEquals(dataClass.rates.aED?.toInt() , 4)
    }
}