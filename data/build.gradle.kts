import org.mrb.paysera.dependencies.Dependencies
plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
    id("kotlin-android-extensions")
    id("kotlin-kapt")
    id("dagger.hilt.android.plugin")

}

android {
    compileSdk = 31

    defaultConfig {
        minSdk = 24
        targetSdk = 31

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {
    implementation(project(mapOf("path" to ":domain")))

    implementation (Dependencies.BaseLibs.coreKtx)
    implementation (Dependencies.BaseLibs.appCompat)
    implementation (Dependencies.BaseLibs.material)
    implementation (Dependencies.BaseLibs.constraintLayout)
    testImplementation (Dependencies.BaseLibs.junitTest)
    androidTestImplementation (Dependencies.Test.junit)
    androidTestImplementation (Dependencies.Test.espresso)

    //HILT
    implementation(Dependencies.Hilt.hilt)
    kapt(Dependencies.Hilt.hiltKaptCompiler)

    //Room
    kapt(Dependencies.Room.roomCompiler)
    implementation(Dependencies.Room.roomKtx)
    implementation(Dependencies.Room.roomRuntime)

    //Retrofit
    implementation(Dependencies.Retrofit.retrofit)
    implementation(Dependencies.Retrofit.rxJavaAdapter)
    implementation(Dependencies.Retrofit.gsonConvertor)
    implementation(Dependencies.Retrofit.loggingInterceptor)




}