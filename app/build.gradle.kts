import org.mrb.paysera.dependencies.Dependencies
plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("kotlin-kapt")
    id("androidx.navigation.safeargs.kotlin")
    id("dagger.hilt.android.plugin")
    id("kotlin-android-extensions")

}

android {
    compileSdk = 31

    defaultConfig {
        applicationId = "org.mrb.paysera"
        minSdk = 24
        targetSdk = 31
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }



    buildTypes {
        named("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
        named("debug") {
            isDebuggable = true
            isMinifyEnabled = false
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        viewBinding = true
    }
}

dependencies {
    implementation(project(mapOf("path" to ":domain")))
    implementation(project(mapOf("path" to ":data")))


    implementation (Dependencies.BaseLibs.coreKtx)
    implementation (Dependencies.BaseLibs.appCompat)
    implementation (Dependencies.BaseLibs.material)
    implementation (Dependencies.BaseLibs.constraintLayout)
    implementation(Dependencies.BaseLibs.fragment)
    implementation(Dependencies.BaseLibs.viewBinding)
    implementation(Dependencies.BaseLibs.kotlinStdLib)

    testImplementation (Dependencies.BaseLibs.junitTest)
    androidTestImplementation (Dependencies.Test.junit)
    androidTestImplementation (Dependencies.Test.espresso)

    //Hilt
    implementation(Dependencies.Hilt.hilt)
    kapt(Dependencies.Hilt.hiltKaptCompiler)
    implementation(Dependencies.Hilt.hiltViewModel)
    kapt(Dependencies.Hilt.hiltViewModelKapt)
    implementation(Dependencies.Hilt.hiltViewModelNavigation)

    //LifeCycle
    implementation(Dependencies.LifeCycle.commonJava8)
    implementation(Dependencies.LifeCycle.runTime)
    implementation(Dependencies.LifeCycle.liveData)
    implementation(Dependencies.LifeCycle.viewModel)

    //Navigation
    implementation(Dependencies.Navigation.navigationUIKtx)
    implementation(Dependencies.Navigation.navigationFragmentKtx)
    implementation(Dependencies.Navigation.navigationDynamicFeatures)

}