package org.mrb.paysera.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import org.mrb.domain.models.MResult
import org.mrb.domain.models.Rate
import org.mrb.domain.useCase.BaseUseCase
import org.mrb.domain.useCase.GetRatesUseCase

@Module
@InstallIn(ViewModelComponent::class)
abstract class UseCaseModule {

    @ViewModelScoped
    @Binds
    abstract fun provideExchangeUseCase(getRatesUseCase: GetRatesUseCase): BaseUseCase<Unit, Flow<MResult<Rate?>>>
}