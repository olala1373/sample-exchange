package org.mrb.paysera.tools

import android.app.Activity
import android.content.Context
import android.text.TextUtils
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import org.mrb.domain.models.MResult
import java.math.BigDecimal
import java.math.RoundingMode

inline fun <reified T> MResult<T>.isSuccess(callback: (data: T?) -> Unit) {
    if (this is MResult.Success) {
        callback.invoke(this.data)
    }
}

inline fun <reified T> MResult<T>.isLoading(callback: (data: T?) -> Unit) {
    if (this is MResult.Loading) {
        callback.invoke(data)
    }
}

inline fun <reified T> MResult<T>.isError(callback: (message: String, code: Int) -> Unit) {
    if (this is MResult.Error) {
        callback.invoke(this.error?.throwable?.message.toString(), this.error?.code ?: 0)
    }
}

fun Context.toast(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}

fun Activity.toast(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}

fun Fragment.toast(msg: String) {
    Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show()
}


fun Double.formatter(decimalValue: Int): Double {
    return BigDecimal(this).setScale(decimalValue, RoundingMode.CEILING).toDouble()
}

fun AutoCompleteTextView.showDropdown(adapter: ArrayAdapter<String>?) {
    if (!TextUtils.isEmpty(this.text.toString())) {
        adapter?.filter?.filter(null)
    }
}