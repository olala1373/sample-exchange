package org.mrb.paysera.tools

object CurrencyConverter {

    fun sellConvert( sellAmount : Double , rate : Double) = (sellAmount * rate).formatter(2)

    fun receiveConvert( receiveAmount : Double , rate : Double) = (receiveAmount / rate).formatter(2)
}