package org.mrb.paysera.core

import android.app.Application
import android.os.StrictMode
import dagger.hilt.android.HiltAndroidApp
import org.mrb.paysera.BuildConfig

@HiltAndroidApp
class PaySeraApp : Application() {
    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG)
            StrictMode.enableDefaults()
    }
}