package org.mrb.paysera.ui.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import org.mrb.data.database.entity.CurrencyEntity
import org.mrb.data.database.SharedPref
import org.mrb.data.database.entity.HistoryEntity
import org.mrb.data.di.IoDispatcher
import org.mrb.data.repository.CurrencyRepository
import org.mrb.data.repository.HistoryRepository
import org.mrb.data.utils.Constants
import org.mrb.domain.models.Rate
import org.mrb.domain.useCase.GetRatesUseCase
import org.mrb.paysera.tools.isError
import org.mrb.paysera.tools.isSuccess
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val useCase: GetRatesUseCase,
    private val currencyRepository: CurrencyRepository,
    @IoDispatcher private val dispatcher: CoroutineDispatcher,
    private val historyRepository: HistoryRepository,
    private val sharedPref: SharedPref
) : ViewModel() {

    private val _screenState: MutableStateFlow<RatesState> =
        MutableStateFlow(RatesState.Success(null))
    val screenState: StateFlow<RatesState> = _screenState
    var balance : Double? = null

    var balanceRemaining = Constants.AppConfig.DEFAULT_BALANCE.toDouble()

    init {
        balance = sharedPref.getBalance()
        if (balance == null || balance == 0.0){
            sharedPref.setBalance(1000.0)
            balance = sharedPref.getBalance()
        }

        viewModelScope.launch {
            while (viewModelScope.isActive) {
                useCase.execute(Unit).collect { res ->
                    res.isSuccess {
                        _screenState.value = RatesState.Success(it)
                    }
                    res.isError { _, code ->
                        _screenState.value = RatesState.Error(code)
                    }
                }
                delay(Constants.AppConfig.API_CALL_INTERVAL)//Change to 5 Seconds
            }
        }
    }


    fun saveBalance(balance : Double){
        sharedPref.setBalance(balance)
    }

    fun getCurrencies() = currencyRepository.getCurrencies()

    fun insertCurrency(entity: CurrencyEntity) = CoroutineScope(dispatcher).launch {
        currencyRepository.insertCurrency(entity)
    }

    fun insertDefaultCurrencies() = CoroutineScope(dispatcher).launch {
        val list = Constants.AppConfig.currencyList.map {
            CurrencyEntity(name = it , balance = 0.0)
        }
        currencyRepository.insertCurrencies(list)
    }

    fun addHistory(entity: HistoryEntity) = CoroutineScope(dispatcher).launch{
        historyRepository.insertHistory(entity)
        currencyRepository.updateCurrencyBalanceByName(entity.toCurrency.uppercase() , entity.toAmount)
    }

    fun getAllHistories() = historyRepository.getHistories()

    fun getTotalTransactionCount() : LiveData<Int> {
        return flow {
            historyRepository.getHistories().collect{
                emit(it.size)
            }
        }.asLiveData()
    }

    fun getBalanceTraded() : LiveData<Double>{
        return flow {
            historyRepository.getBalanceTraded().collect{
                it?.let {
                    emit(it)
                }
            }
        }.asLiveData()
    }
}

sealed class RatesState {
    data class Success(val rate: Rate?) : RatesState()
    data class Error(val code: Int) : RatesState()
}
