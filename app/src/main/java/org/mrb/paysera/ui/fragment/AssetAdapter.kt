package org.mrb.paysera.ui.fragment

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.imageview.ShapeableImageView
import org.mrb.data.database.entity.CurrencyEntity
import org.mrb.paysera.R

class AssetAdapter(private val assets: List<CurrencyEntity>) :
    RecyclerView.Adapter<AssetAdapter.AssetViewHolder>() {


    inner class AssetViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val indicator: ShapeableImageView = itemView.findViewById(R.id.assetIndicator)
        private val textIndicator: TextView = itemView.findViewById(R.id.textIndicator)
        private val assetName: TextView = itemView.findViewById(R.id.assetName)
        private val assetBalance: TextView = itemView.findViewById(R.id.assetBalance)
        fun bind(asset: CurrencyEntity) {
            assetName.text = asset.name.uppercase()
            assetBalance.text = asset.balance.toString()
            textIndicator.text = asset.name.first().toString().uppercase()
            when (adapterPosition % 4) {
                0 -> indicator.backgroundTintList = ColorStateList.valueOf(
                    ContextCompat.getColor(
                        itemView.context,
                        R.color.indicator1
                    )
                )
                1 -> indicator.backgroundTintList = ColorStateList.valueOf(
                    ContextCompat.getColor(
                        itemView.context,
                        R.color.indicator2
                    )
                )
                2 -> indicator.backgroundTintList = ColorStateList.valueOf(
                    ContextCompat.getColor(
                        itemView.context,
                        R.color.indicator3
                    )
                )
                3 -> indicator.backgroundTintList = ColorStateList.valueOf(
                    ContextCompat.getColor(
                        itemView.context,
                        R.color.indicator4
                    )
                )
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AssetViewHolder {
        return AssetViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.asset_item_layout, parent, false)
        )
    }

    override fun onBindViewHolder(holder: AssetViewHolder, position: Int) {
        holder.bind(assets[position])
    }

    override fun getItemCount(): Int = assets.size
}