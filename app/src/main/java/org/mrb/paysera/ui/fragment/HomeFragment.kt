package org.mrb.paysera.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import org.mrb.data.utils.Constants
import org.mrb.paysera.databinding.HomeFragmentLayoutBinding
import org.mrb.paysera.tools.formatter
import org.mrb.paysera.ui.viewModel.HomeViewModel
import org.mrb.paysera.ui.viewModel.RatesState

@AndroidEntryPoint
class HomeFragment : Fragment() {

    private val viewModel: HomeViewModel by activityViewModels {
        defaultViewModelProviderFactory
    }
    private var _binding: HomeFragmentLayoutBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = HomeFragmentLayoutBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.balance.text = "${viewModel.balanceRemaining} ${Constants.AppConfig.BASE}"

        lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED){
                launch {
                    viewModel.screenState.collect{
                            screenState->
                        when(screenState){
                            is RatesState.Success -> {

                            }
                            is RatesState.Error -> {
                                if (screenState.code == 401){
                                    Toast.makeText(requireContext(), "Invalid access token", Toast.LENGTH_SHORT).show()
                                }
                            }
                        }
                    }
                }
                launch {
                    viewModel.getCurrencies().collect{
                        if (it.isEmpty()){
                           viewModel.insertDefaultCurrencies()
                        }else{
                            binding.assetList.adapter = AssetAdapter(it)
                        }
                    }
                }
            }
        }

        viewModel.getBalanceTraded().observe(viewLifecycleOwner){
            viewModel.balanceRemaining = (Constants.AppConfig.DEFAULT_BALANCE.toDouble() - it).formatter(2)
            binding.balance.text = "${viewModel.balanceRemaining} ${Constants.AppConfig.BASE}"
        }
    }

}