package org.mrb.paysera.ui.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.android.synthetic.main.exchange_fragment_layout.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import org.mrb.data.database.entity.HistoryEntity
import org.mrb.data.utils.Constants
import org.mrb.domain.models.RateItem
import org.mrb.paysera.R
import org.mrb.paysera.databinding.ExchangeFragmentLayoutBinding
import org.mrb.paysera.tools.CurrencyConverter
import org.mrb.paysera.tools.formatter
import org.mrb.paysera.tools.showDropdown
import org.mrb.paysera.tools.toast
import org.mrb.paysera.ui.viewModel.HomeViewModel
import org.mrb.paysera.ui.viewModel.RatesState
import java.util.*

@AndroidEntryPoint
class ExchangeFragment : Fragment() {
    private val viewModel: HomeViewModel by activityViewModels {
        defaultViewModelProviderFactory
    }
    private var _binding: ExchangeFragmentLayoutBinding? = null
    private val binding get() = _binding!!
    private var totalTransactionMade = 0
    private lateinit var sellCurrency: String
    private lateinit var receiveCurrency: String
    private lateinit var rateItems: List<RateItem>
    private val receiveList = Constants.AppConfig.currencyList
    private var adapter : ArrayAdapter<String>? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = ExchangeFragmentLayoutBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rateItems = emptyList()
        initViews()
        lifecycleScope.launch {
            launch {
                viewModel.getCurrencies().flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
                    .onEach {
                        val list = it.filter { entity -> entity.balance > 0.0 }
                            .map { entity -> entity.name } + Constants.AppConfig.BASE
                        initDropDowns(list)
                    }.collect()
            }
            launch {
                viewModel.screenState.flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
                    .onEach {
                        when (it) {
                            is RatesState.Success -> {
                                it.rate?.let { rate ->
                                    Log.d(TAG, rate.toString())
                                    rateItems = rate.items
                                    convert()
                                }
                            }
                            is RatesState.Error -> {

                            }
                        }
                    }.collect()
            }
        }

        viewModel.getTotalTransactionCount().observe(viewLifecycleOwner){
            totalTransactionMade = it
        }

        viewModel.getBalanceTraded().observe(viewLifecycleOwner){
            viewModel.balanceRemaining = (Constants.AppConfig.DEFAULT_BALANCE.toDouble() - it).formatter(2)
            binding.balance.text = "${viewModel.balanceRemaining} ${Constants.AppConfig.BASE}"
        }
    }

    override fun onPause() {
        super.onPause()
        viewModel.saveBalance(viewModel.balanceRemaining)
    }

    private fun initDropDowns(sellList : List<String>){
        binding.sellCurrencyList.setText(Constants.AppConfig.BASE)
        binding.sellCurrencyList.setAdapter(
            ArrayAdapter(
                requireContext(),
                com.google.android.material.R.layout.support_simple_spinner_dropdown_item,
                sellList.reversed()
            )
        )
        val adapter = ArrayAdapter(
            requireContext(),
            com.google.android.material.R.layout.support_simple_spinner_dropdown_item,
            Constants.AppConfig.currencyList
        )
        binding.receiveCurrencyList.setText(Constants.AppConfig.currencyList.first() , false)
        binding.receiveCurrencyList.setAdapter(adapter)
    }

    private fun initViews() {

        binding.balance.text = "${viewModel.balanceRemaining} ${Constants.AppConfig.BASE}"

        binding.submitButton.isEnabled = false

        sellCurrency = binding.sellCurrencyList.text.toString()
        receiveCurrency = binding.receiveCurrencyList.text.toString()


        binding.sellCurrencyList.doAfterTextChanged {
            sellCurrency = it.toString()
            if (sellCurrency.lowercase() != Constants.AppConfig.BASE.lowercase()) {
                binding.sellErrorText.text = getString(R.string.rate_unavailable)
            }else{
                binding.sellErrorText.text = ""
            }
        }
        binding.receiveCurrencyList.doAfterTextChanged {
            receiveCurrency = it.toString()
        }

        binding.submitButton.setOnClickListener {
            if (Constants.AppConfig.DEFAULT_COMMISSION_FREE > totalTransactionMade){
                makeHistoryObject(0.0)
            }else{
                makeHistoryObject(0.7)
            }
            binding.sellInput.setText("")
            binding.receieveInput.setText("")
        }
    }

    private fun makeHistoryObject(fee : Double){
        val sellAmount = binding.sellInput.text.toString().toDouble().formatter(2)
        val receiveAmount = binding.receieveInput.text.toString().toDouble().formatter(2)

        if (sellAmount > viewModel.balanceRemaining){
            toast("You don't have enough balance")
        }else{
            val entity = HistoryEntity(
                fromCurrency = sellCurrency,
                toCurrency = receiveCurrency,
                fromAmount = sellAmount,
                toAmount = receiveAmount,
                fee = fee,
                createdDate = Date().toString(),
                rate = getRateLive()
            )

            viewModel.addHistory(entity)
            with(entity){
                toast("You converted $fromAmount $fromCurrency to $toAmount $toCurrency. Your Commission fee is $fee $fromCurrency")
            }
        }

    }

    private fun convert() {
        binding.sellInput.doAfterTextChanged {
            if (binding.sellInput.isFocused) {
                try {
                    if (it.toString().isNotEmpty() && !binding.submitButton.isEnabled) binding.submitButton.isEnabled = true
                    val amount = CurrencyConverter.sellConvert(
                        it.toString().toDouble(),
                        getRateLive()
                    )
                    binding.receieveInput.setText(amount.toString())
                } catch (e: Exception) {
                    if (binding.sellInput.text.toString().isNotEmpty())
                        binding.sellErrorText.text = getString(R.string.inavlid_input)
                    binding.receieveInput.setText("")
                }
            }
        }

        binding.receiveCurrencyList.doAfterTextChanged {
            try {
                binding.receieveInput.setText(
                    CurrencyConverter.sellConvert(
                        binding.sellInput.text.toString().toDouble(),
                        getRateLive()
                    ).toString()
                )
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        binding.receieveInput.doAfterTextChanged {
            if (binding.receieveInput.isFocused) {
                try {
                    val amount =
                        CurrencyConverter.receiveConvert(it.toString().toDouble(), getRateLive())
                    binding.sellInput.setText(amount.toString())
                } catch (e: Exception) {
                    if (binding.receieveInput.text.toString().isNotEmpty())
                        binding.receiveErrorText.text = getString(R.string.inavlid_input)
                    binding.sellInput.setText("")
                }
            }

        }
    }

    private fun getRateLive(): Double =
        rateItems.find { rateItem -> rateItem.name.lowercase() == receiveCurrency.lowercase() }?.value
            ?: 0.0

    companion object {
        const val TAG = "EXCHANGE TAG"
    }
}